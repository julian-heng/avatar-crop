import React from "react";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";

import "./Sidebar.css"


type SidebarProps = {
    circle: boolean;

    inputClickCallback: () => void;
    circleSetCallback: (newCircle: boolean) => void;
    cropSaveCallback: () => void;
}


class Sidebar extends React.Component<SidebarProps>
{
    render()
    {
        return (
            <Container
                fluid
                className="sidebar bg-primary h-100"
            >
                <Row className="pt-2 no-gutters">
                    <Col>
                        <Button
                            className="btn-block"
                            onClick={this.props.inputClickCallback}
                        >
                            Open Image
                        </Button>
                    </Col>
                    <Col>
                        <Button
                            className="btn-block"
                            onClick={this.props.cropSaveCallback}
                        >
                            Save Crop
                        </Button>
                    </Col>
                </Row>
                <Row className="no-gutters">
                    <Col>
                        <Button
                            className="btn-block"
                            onClick={() => {
                                let newCircle = !this.props.circle;
                                this.props.circleSetCallback(newCircle);
                            }}
                        >
                            {this.props.circle ? "Circle" : "Square"}
                        </Button>
                    </Col>
                </Row>
            </Container>
        );
    }
}


export default Sidebar
