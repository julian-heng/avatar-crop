import React from "react";
import ReactCrop from "react-image-crop";
import { saveAs } from "file-saver";

import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";

import "./App.css";
import AvatarCropper from "./AvatarCropper";
import AvatarPreview from "./AvatarPreview";
import Sidebar from "./Sidebar";

import "bootstrap/dist/css/bootstrap.min.css";


type AppState = {
    img: string;
    circle: boolean;
    crop: ReactCrop.Crop;
    imgCrop: string;
    imgRef: HTMLImageElement | undefined;
};


class App extends React.Component<{}, AppState>
{
    static defaultState: AppState = {
        img: "",
        circle: true,
        crop: {},
        imgCrop: "",
        imgRef: undefined,
    };

    constructor(props: Object)
    {
        super(props);

        this.state = App.defaultState;

        this.activateInput = this.activateInput.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.cropUpdate = this.cropUpdate.bind(this);
        this.cropSave = this.cropSave.bind(this);
    }

    render()
    {
        return (
            <Container
                fluid
                className="App"
            >
                <Row className="h-100 no-gutters">
                    <Sidebar
                        circle={this.state.circle}
                        inputClickCallback={this.activateInput}
                        circleSetCallback={(newCircle: boolean) => {
                            this.setState({circle: newCircle});
                        }}
                        cropSaveCallback={this.cropSave}
                    />
                    <Col>
                        <AvatarCropper
                            style={{
                                height: "calc(100% - 144px)",
                            }}
                            img={this.state.img}
                            circle={this.state.circle}
                            inputClickCallback={this.activateInput}
                            cropUpdateCallback={this.cropUpdate}
                            imgRefUpdateCallback={
                                (newImgRef: HTMLImageElement | undefined) => {
                                    this.setState({imgRef: newImgRef});
                                }
                            }
                        />
                        <AvatarPreview
                            img={this.state.imgCrop}
                            crop={this.state.crop}
                            circle={this.state.circle}
                        />
                    </Col>
                </Row>
                <input
                    ref="input"
                    className="d-none"
                    type="file"
                    accept="image/png, image/jpeg"
                    onChange={this.handleInputChange}
                />
            </Container>
        );
    }

    private activateInput(): void
    {
        let input = this.refs.input as HTMLInputElement;
        input.click();
    }

    private handleInputChange(): void
    {
        let input = this.refs.input as HTMLInputElement;

        if (!input.files || !input.files[0])
        {
            return;
        }

        let file = input.files[0];

        let reader = new FileReader();
        reader.addEventListener("load", (evt: Event) => {
            if (evt === null ||
                evt.target === null ||
                reader.result == null)
            {
                return;
            }

            let data = reader.result as ArrayBuffer;
            let blob = new Blob([data], {
                type: "image/png"
            });
            let url = URL.createObjectURL(blob);
            this.setState({img: url});
        });

        reader.readAsArrayBuffer(file);
    }

    private cropUpdate(newCrop: ReactCrop.Crop): void
    {
        this.setState({crop: newCrop});

        let imgRef = this.state.imgRef;
        let crop = this.state.crop;

        if (imgRef === undefined ||
            crop === undefined ||
            crop.x === undefined ||
            crop.y === undefined ||
            crop.width === undefined ||
            crop.height === undefined)
        {
            return;
        }

        let sx = imgRef.naturalWidth / imgRef.width;
        let sy = imgRef.naturalHeight / imgRef.height;

        let canvas = document.createElement("canvas");
        canvas.width = crop.width * sx;
        canvas.height = crop.height * sy;

        let ctx = canvas.getContext("2d");

        if (ctx === null)
        {
            // Should be better handled
            return;
        }

        let cx = crop.x * sx;
        let cy = crop.y * sy;
        let cw = crop.width * sx;
        let ch = crop.height * sy;

        ctx.drawImage(imgRef, cx, cy, cw, ch, 0, 0, cw, ch);
        canvas.toBlob((blob) => {
            if (blob !== null)
            {
                let url = URL.createObjectURL(blob);
                this.setState({imgCrop: url});
            }
        });
    }

    private cropSave(): void
    {
        saveAs(this.state.imgCrop, "cropped.png");
    }
}


export default App;
