import React from "react";
import ReactCrop from "react-image-crop";

import "react-image-crop/dist/ReactCrop.css";

import "./AvatarCropper.css"

import { Dimension } from "./Common"


type AvatarCropperProps = {
    style: React.CSSProperties,
    img: string;
    circle: boolean;
    inputClickCallback: () => void;
    cropUpdateCallback: (newCrop: ReactCrop.Crop) => void;
    imgRefUpdateCallback:
        (newImgRef: HTMLImageElement | undefined) => void;
};


type AvatarCropperState = {
    crop: ReactCrop.Crop;
    imgStats: Dimension;
    imgRef: HTMLImageElement | undefined;
};


class AvatarCropper extends React.Component<AvatarCropperProps,
                                            AvatarCropperState>
{
    static defaultState: AvatarCropperState = {
        crop: {
            unit: "px",
            x: 0,
            y: 0,
            width: 256,
            height: 256,
        },
        imgStats: {
            width: 0,
            height: 0,
        },
        imgRef: undefined,
    };

    constructor(props: AvatarCropperProps)
    {
        super(props);
        this.state = AvatarCropper.defaultState;

        this.onResize = this.onResize.bind(this);
        this.onClick = this.onClick.bind(this);
        this.onCropChange = this.onCropChange.bind(this);
        this.onImageLoaded = this.onImageLoaded.bind(this);
    }

    componentDidMount()
    {
        this.onResize();
        window.addEventListener("resize", this.onResize);
    }

    componentDidUnmount()
    {
        window.removeEventListener("resize", this.onResize);
    }

    render()
    {
        return (
            <div
                className="avatar-root"
                style={this.props.style}
            >
                {this.props.img === ""
                ?
                    <div
                        className="avatar-container-placeholder"
                        onClick={this.onClick}
                    >
                        <div className="avatar-container-placeholder-text">
                            Click Here or Open Image...
                        </div>
                    </div>
                :
                    <ReactCrop
                        className="avatar-container"
                        src={this.props.img}
                        crop={this.state.crop}
                        onChange={this.onCropChange}
                        maxWidth={this.state.imgStats.width}
                        maxHeight={this.state.imgStats.height}
                        keepSelection
                        onImageLoaded={this.onImageLoaded}
                        circularCrop={this.props.circle}
                    />
                }
            </div>
        );
    }

    private onResize(): void
    {
        let imgRef = this.state.imgRef;

        if (imgRef === undefined)
        {
            return;
        }

        this.setState({
            imgStats: {
                width: imgRef.width,
                height: imgRef.height,
            },
        });
    }

    private onClick(): void
    {
        if (this.props.img === "")
        {
            this.props.inputClickCallback();
        }
    }

    private onCropChange(newCrop: ReactCrop.Crop): void
    {
        newCrop = Object.assign({}, newCrop, {aspect: 1});
        this.setState({crop: newCrop});
        this.props.cropUpdateCallback(newCrop);
    }

    private onImageLoaded(newImgRef: HTMLImageElement): boolean
    {
        // Pass img attribute to callback
        this.props.imgRefUpdateCallback(newImgRef);

        // Set initial image viewport dimensions as well as the appropriate
        // crop size
        let crop = this.state.crop;
        if (crop !== undefined && crop.width !== undefined)
        {
            let cropLength = Math.min(
                ...[crop.width, newImgRef.width, newImgRef.height]
            );
            crop = Object.assign({}, crop, {
                width: cropLength,
                height: cropLength,
            });

            this.onCropChange(crop);
        }

        this.setState({
            imgStats: {
                width: newImgRef.width,
                height: newImgRef.height,
            },
            imgRef: newImgRef,
        });

        // Return false to update crop in ReactCrop
        return false;
    }
}


export default AvatarCropper;
