import React from "react";
import ReactCrop from "react-image-crop";

import "./AvatarPreview.css"

import { Dimension } from "./Common"


type AvatarPreviewProps = {
    img: string;
    crop: ReactCrop.Crop;
    circle: boolean;
};


class AvatarPreview extends React.Component<AvatarPreviewProps>
{
    static defaultSizes: Array<Dimension> = [
        { width: 128, height: 128 },
        { width: 64, height: 64 },
        { width: 48, height: 48 },
        { width: 40, height: 40 },
        { width: 32, height: 32 },
    ];

    constructor(props: AvatarPreviewProps)
    {
        super(props);

        this.createPreview = this.createPreview.bind(this);
    }

    render()
    {
        return (
            <div className="avatar-preview-root">
                <div className="avatar-preview-container">
                    {this.props.img === "" ? <div/> : AvatarPreview.defaultSizes.map(this.createPreview)}
                </div>
            </div>
        );
    }

    private createPreview(size: Dimension): React.ReactNode
    {
        let { width: w, height: h } = size;

        let style = {
            width: `${w}px`,
            height: `${h}px`,
            borderRadius: this.props.circle ? "50%" : "0%",
            border: "1px solid",
        };

        return (
            <div className="avatar-preview-cell">
                <img
                    style={style}
                    src={this.props.img}
                    title={`${w}x${h}`}
                    alt=""
                />
            </div>
        )
    }
}


export default AvatarPreview;
